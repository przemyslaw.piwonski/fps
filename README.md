# Final Project Store - Online Shop.  
This online shop is final project in Software Development Academy. The aim of this work is to summarize everything we have learned during the nearly one year course. It allowed us to see what real work looks like.

## Technologies
1. Java: 11
2. Spring Boot: 2.4.0  
    * Validations  
    * Security  
    * Data JPA  
    * Thymeleaf  
7. PostgreSQL: 13.1-alpine
8. Bootstrap: 4