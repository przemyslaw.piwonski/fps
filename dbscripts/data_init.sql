insert into public.category (id, name, slug)
values (1, 'Gry Komputerowe', 'gry-komputerowe');
insert into public.category (id, name, slug)
values (2, 'Gry Konsolowe', 'gry-konsoloweu');
insert into public.sub_category (id, name, slug, parent_id)
values (1, 'RPG', 'rpg', 1);
insert into public.sub_category (id, name, slug, parent_id)
values (2, 'Bijatyki', 'bijatyki', 1);
insert into public.product (id, name, url_photo, price, description, category_id)
values (1, 'Fallout', 'https://www.miastogier.pl/baza/Encyklopedia/gry/Fallout_PC/Okladka/okl_falloutcover.jpg',
        19.99, 'Fallout jest nieoficjalnym sequelem gry wydanej w 1987 roku, znanej chyba każdemu fanowi cRPG - Wasteland. ' ||
               'Akcja osadzona została w świecie zniszczonym przez wielką atomową wojnę. ' ||
               'Cała rozwinięta cywilizacja została niemal zmieciona z powierzchni planety, ' ||
               'a te niedobitki, które przeżyły zmuszone są żyć w całkowicie zrujnowanym świecie pełnym mutantów i ' ||
               'niebezpiecznego promieniowania. Jedynymi bezpiecznymi miejscami, gdzie tworzą się większe osady i ' ||
               'skupiska ludzkie są piwnice, groty, schrony i tym podobne miejsca. W jednym z takich miejsc, schronie ' ||
               '13 wyrósł również nasz bohater. Cała społeczność tzw. vaultu żyła w spokoju, aż do dnia, w którym zepsuł ' ||
               'się chip odpowiedzialny za produkcję czystej wody. Istnienie wszystkich mieszkańców zostało zagrożone, ' ||
               'a my zostaliśmy wybrani do wyjścia na powierzchnię i odnalezienia nowego chipa. Czeka nas niesamowita podróż ' ||
               'po apokaliptycznym świecie, wiele siedlisk ludzkich do zwiedzenia, wiele walk, ogólnie wszystko to co jest ' ||
               'kwintesencją gatunku cRPG.', 1);
insert into public.product (id, name, url_photo, price, description, category_id)
values (2, 'Mortal Kombat X', 'https://cdn.gracza.pl/galeria/gry13/grupy/15692.jpg', 169.9,
        'Mortal Kombat X to dziesiąta odsłona kultowej serii brutalnych bijatyk, ' ||
        'która istnieje od 1992 roku, a zarazem druga gra z tego cyklu w dorobku studia NetherRealm. ' ||
        'Innymi słowy, mamy tu do czynienia z kontynuacją bardzo udanego Mortal Kombat z 2011 roku.
Źródło: https://www.gry-online.pl/gry/mortal-kombat-x/ze3d4c', 2);

insert into public.product (id, name, url_photo, price, description, category_id)
values (3, 'Cyberpunk 2077', 'https://www.gry-online.pl/galeria/gry13/388894437.jpg', 169.9,
        'Cyberpunk 2077 jest pierwszoosobową grą RPG z otwartym światem, wzbogaconą o elementy FPS-ów. ' ||
        'Za jej stworzenie odpowiada polskie studio CD Projekt RED, które zdobyło międzynarodową sławę dzięki ' ||
        'bestsellerowemu cyklowi Wiedźmin. ' ||
        'Tytuł oparto na licencji Cyberpunka 2020 – gry fabularnej stworzonej w 1990 roku przez Mike’a Pondsmitha.', 1);
