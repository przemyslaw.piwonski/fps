package pl.sdacademy.fpsapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class FpsAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(FpsAppApplication.class, args);
	}

}
