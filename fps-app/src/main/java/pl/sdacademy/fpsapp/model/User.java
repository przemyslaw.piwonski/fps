package pl.sdacademy.fpsapp.model;

import pl.sdacademy.fpsapp.domain.address.Address;
import pl.sdacademy.fpsapp.model.enums.Role;

import javax.persistence.*;

@Entity
@Table(name = "shop_user")
@SequenceGenerator(name = "shopUserSeq", sequenceName = "shop_user_id_seq", allocationSize = 1)
public class  User {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "shopUserSeq")
    private Integer id;
    private String login;
    private String password;
    private String address;
    @Enumerated(value = EnumType.STRING)
    private Role role;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address.toString();
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
