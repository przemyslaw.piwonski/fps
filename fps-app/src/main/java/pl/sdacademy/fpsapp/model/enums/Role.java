package pl.sdacademy.fpsapp.model.enums;

public enum Role {
    USER,
    ADMIN
}
