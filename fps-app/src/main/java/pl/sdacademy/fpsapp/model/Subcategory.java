package pl.sdacademy.fpsapp.model;

import javax.persistence.*;

@Entity
@Table(name = "sub_category")
@SequenceGenerator(name = "subCategorySeq", sequenceName = "sub_category_id_seq", allocationSize = 1)
public class Subcategory {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "subCategorySeq")
    private Integer id;
    private String name;
    private String slug;

    @ManyToOne
    @JoinColumn(name = "parent_id")
    private Category parentCategory;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(Category parentCategory) {
        this.parentCategory = parentCategory;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
}
