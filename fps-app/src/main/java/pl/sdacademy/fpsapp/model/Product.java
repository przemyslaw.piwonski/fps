package pl.sdacademy.fpsapp.model;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "product")
@SequenceGenerator(name = "productSeq", sequenceName = "product_id_seq", allocationSize = 1)
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "productSeq")
    private Integer id;
    private String name;
    @Column(name = "url_photo")
    private String urlPhoto;
    private BigDecimal price;
    private String description;
    @ManyToOne
    @JoinColumn(name = "category_id")
    private Subcategory subCategory;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlPhoto() {
        return urlPhoto;
    }

    public void setUrlPhoto(String url_photo) {
        this.urlPhoto = url_photo;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Subcategory getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(Subcategory category) {
        this.subCategory = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
