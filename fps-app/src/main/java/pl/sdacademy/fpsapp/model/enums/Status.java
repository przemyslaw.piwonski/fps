package pl.sdacademy.fpsapp.model.enums;

public enum Status {
    CONFIRMED,
    WAITING,
    REALIZED
}
