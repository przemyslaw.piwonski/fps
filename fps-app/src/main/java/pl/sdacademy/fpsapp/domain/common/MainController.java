package pl.sdacademy.fpsapp.domain.common;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping(value = "/")
public class MainController {
    private final BasicLayoutProvider basicLayoutProvider;


    public MainController(BasicLayoutProvider basicLayoutProvider) {
        this.basicLayoutProvider = basicLayoutProvider;

    }

    @GetMapping
    public ModelAndView getHomeView() {
        return basicLayoutProvider.getLayoutView("welcome");
    }


}
