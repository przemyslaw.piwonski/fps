package pl.sdacademy.fpsapp.domain.orderline;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sdacademy.fpsapp.model.OrderLine;

public interface OrderLineRepository extends JpaRepository<OrderLine, Integer> {
}
