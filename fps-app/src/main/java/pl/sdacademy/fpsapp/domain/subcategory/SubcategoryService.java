package pl.sdacademy.fpsapp.domain.subcategory;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import pl.sdacademy.fpsapp.model.Subcategory;

import java.util.List;
import java.util.Optional;

@Service
public class SubcategoryService {
    private final SubcategoryRepository subCategoryRepository;

    public SubcategoryService(SubcategoryRepository subCategoryRepository) {
        this.subCategoryRepository = subCategoryRepository;
    }

    public Subcategory addSubCategory(Subcategory subCategory){
        return subCategoryRepository.save(subCategory);
    }

    @Cacheable("subcategories")
    public List<Subcategory> getSubcategories(String parentCategorySlug) {
        return subCategoryRepository.findAllByParentCategory_Slug(parentCategorySlug);
    }

    @Cacheable("subcategory")
    public Optional<Subcategory> getSubcategory(String parentCategorySlug, String subCatSlug) {
        return subCategoryRepository.findByParentCategory_SlugAndSlug(parentCategorySlug, subCatSlug);
    }
}
