package pl.sdacademy.fpsapp.domain.shoppingcart;

import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;
import org.springframework.web.server.ResponseStatusException;
import pl.sdacademy.fpsapp.model.OrderLine;
import pl.sdacademy.fpsapp.model.Product;

import java.math.BigDecimal;
import java.util.*;

@Component
@SessionScope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ShoppingCart {
    private List<OrderLine> orderLineList = Collections.synchronizedList(new LinkedList<>());

    public List<OrderLine> getOrderLineList() {
        return orderLineList;
    }

    public boolean isProductPresent(Integer productId) {
        if (orderLineList.isEmpty()) {
            return false;
        }
        return orderLineList.stream()
                .anyMatch(orderLine -> orderLine.getProduct().getId().equals(productId));
    }

    public BigDecimal getSummaryPrice() {
        BigDecimal summaryPrice = new BigDecimal(0);
        for (OrderLine orderLine : orderLineList) {
            summaryPrice = summaryPrice.add(
                    orderLine.getPrice().multiply(BigDecimal.valueOf(orderLine.getQuantity())));
        }
        return summaryPrice;
    }

    public void addOrderLine(Product product) {
        if (isProductPresent(product.getId())) {
            incQuantity(product.getId());
        } else {
            OrderLine orderLine = new OrderLine();
            orderLine.setProduct(product);
            orderLine.setPrice(product.getPrice());
            orderLine.setQuantity(1);
            orderLineList.add(orderLine);
        }
    }

    public void removeOrderLine(Integer productId) {
        OrderLine orderLine = getOrderLine(productId);
        orderLineList.remove(orderLine);
    }

    public void incQuantity(Integer productId) {
        OrderLine orderLine = getOrderLine(productId);
        orderLine.setQuantity(orderLine.getQuantity() + 1);
    }

    public void decQuantity(Integer productId) {
        OrderLine orderLine = getOrderLine(productId);
        if (orderLine.getQuantity() > 1) {
            orderLine.setQuantity(orderLine.getQuantity() - 1);
        }
    }

    private OrderLine getOrderLine(Integer productId) {
        Optional<OrderLine> optionalOrderLine = orderLineList.stream()
                .filter(orderLine -> orderLine.getProduct().getId().equals(productId))
                .findAny();
        return optionalOrderLine.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "asdasdas"));
    }

}
