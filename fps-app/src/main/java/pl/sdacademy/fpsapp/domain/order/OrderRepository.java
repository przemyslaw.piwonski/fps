package pl.sdacademy.fpsapp.domain.order;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sdacademy.fpsapp.model.Order;

public interface OrderRepository extends JpaRepository<Order, Integer> {
}
