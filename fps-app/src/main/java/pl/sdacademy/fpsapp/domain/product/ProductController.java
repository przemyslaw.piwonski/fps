package pl.sdacademy.fpsapp.domain.product;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;
import pl.sdacademy.fpsapp.domain.common.BasicLayoutProvider;
import pl.sdacademy.fpsapp.model.Product;


@Controller
@RequestMapping("/products")
public class ProductController {
    private final ProductService productService;
    private final BasicLayoutProvider basicLayoutProvider;


    public ProductController(ProductService productService, BasicLayoutProvider basicLayoutProvider) {
        this.productService = productService;
        this.basicLayoutProvider = basicLayoutProvider;
    }

    @GetMapping("/{product-id}")
    public ModelAndView getProduct(@PathVariable("product-id") Integer productId) {
        final Product product = productService.getProduct(productId).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, "Nieznalezion strony: /products/" + productId));
        final ModelAndView mav = basicLayoutProvider.getLayoutView("product-details");
        mav.addObject("product", product);
        return mav;
    }
}
