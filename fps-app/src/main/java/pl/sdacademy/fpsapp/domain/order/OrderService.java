package pl.sdacademy.fpsapp.domain.order;

import org.springframework.stereotype.Service;
import pl.sdacademy.fpsapp.domain.address.Address;
import pl.sdacademy.fpsapp.domain.orderline.OrderLineService;
import pl.sdacademy.fpsapp.domain.shoppingcart.ShoppingCart;
import pl.sdacademy.fpsapp.model.Order;
import pl.sdacademy.fpsapp.model.OrderLine;
import pl.sdacademy.fpsapp.model.enums.Status;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class OrderService {
    private final OrderRepository orderRepository;
    private final ShoppingCart shoppingCart;
    private final OrderLineService orderLineService;

    public OrderService(
            OrderRepository orderRepository,
            ShoppingCart shoppingCart,
            OrderLineService orderLineService
    ) {
        this.orderRepository = orderRepository;
        this.shoppingCart = shoppingCart;
        this.orderLineService = orderLineService;
    }

    @Transactional
    public Order addOrder(Address address){
        final Order order = new Order();
        order.setDateTime(LocalDateTime.now());
        order.setDeliveryAddress(address.toString());
        order.setStatus(Status.REALIZED);
        final Order savedOrder = orderRepository.save(order);
        final List<OrderLine> orderLineList = shoppingCart.getOrderLineList();
        orderLineList.forEach(o -> o.setOrder(savedOrder));
        orderLineService.addOrderLineList(orderLineList);
        return savedOrder;
    }





}
