package pl.sdacademy.fpsapp.domain.product;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sdacademy.fpsapp.model.Product;

import java.math.BigDecimal;
import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Integer> {
    List<Product> findAllBySubCategory_Slug(String slug);

    List<Product> findAllBySubCategory_ParentCategory_Slug(String categorySlug);
}
