package pl.sdacademy.fpsapp.domain.subcategory;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sdacademy.fpsapp.model.Subcategory;

import java.util.List;
import java.util.Optional;

public interface SubcategoryRepository extends JpaRepository<Subcategory, Integer> {
    Optional<Subcategory> findByParentCategory_SlugAndSlug(String parentCategorySlug, String subCatSlug);

    List<Subcategory> findAllByParentCategory_Slug(String parentCategorySlug);
}
