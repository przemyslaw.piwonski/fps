package pl.sdacademy.fpsapp.domain.order;


import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.sdacademy.fpsapp.domain.address.Address;
import pl.sdacademy.fpsapp.domain.common.BasicLayoutProvider;
import pl.sdacademy.fpsapp.domain.shoppingcart.ShoppingCart;


import javax.validation.Valid;


@RequestMapping
@Controller
public class OrderController {
    private final ShoppingCart shoppingCart;
    private final OrderService orderService;
    private final BasicLayoutProvider basicLayoutProvider;

    public OrderController(
            ShoppingCart shoppingCart,
            OrderService orderService,
            BasicLayoutProvider basicLayoutProvider
    ) {
        this.shoppingCart = shoppingCart;
        this.orderService = orderService;
        this.basicLayoutProvider = basicLayoutProvider;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @PostMapping("/order")
    public ModelAndView addOrder(@Valid @ModelAttribute Address address, BindingResult bindingResult){
        if (bindingResult.hasErrors()) {
            return basicLayoutProvider.getLayoutView("order-form")
                    .addObject("shoppingCart", shoppingCart);
        }
        orderService.addOrder(address);
        return new ModelAndView("redirect:/thanks");
    }

    @GetMapping("/order")
    public ModelAndView orderForm(@ModelAttribute Address address){
        final ModelAndView mav = basicLayoutProvider.getLayoutView("order-form");
        if(shoppingCart.getOrderLineList().isEmpty()){
            mav.setViewName("redirect:/shopping-cart");
            return mav;
        }
        mav.addObject("shoppingCart", shoppingCart);
        return mav;
    }

    @GetMapping("/thanks")
    public ModelAndView thanksPage() {
        final ModelAndView mav = basicLayoutProvider.getLayoutView("thank-you-page");
        if(shoppingCart.getOrderLineList().isEmpty()){
            mav.setViewName("redirect:/shopping-cart");
            return mav;
        }
        shoppingCart.getOrderLineList().clear();
        return mav;
    }
}
