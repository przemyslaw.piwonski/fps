package pl.sdacademy.fpsapp.domain.category;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;
import pl.sdacademy.fpsapp.domain.common.BasicLayoutProvider;
import pl.sdacademy.fpsapp.domain.product.ProductService;
import pl.sdacademy.fpsapp.domain.subcategory.SubcategoryService;
import pl.sdacademy.fpsapp.model.Category;
import pl.sdacademy.fpsapp.model.Product;
import pl.sdacademy.fpsapp.model.Subcategory;

import java.util.List;

@Controller
@RequestMapping("/categories")
public class CategoryController {
    private final ProductService productService;
    private final SubcategoryService subcategoryService;
    private final BasicLayoutProvider basicLayoutProvider;
    private final CategoryService categoryService;

    public CategoryController(ProductService productService, SubcategoryService subcategoryService, BasicLayoutProvider basicLayoutProvider, CategoryService categoryService) {
        this.productService = productService;
        this.subcategoryService = subcategoryService;
        this.basicLayoutProvider = basicLayoutProvider;
        this.categoryService = categoryService;
    }

    @GetMapping({ "/{category-slug}", "/{category-slug}/{sub-category-slug}"})
    public ModelAndView getProductsByCategory(
            @PathVariable("category-slug") String catSlug,
            @PathVariable(value = "sub-category-slug", required = false) String subcatSlug
    ) {
        final ModelAndView mav = basicLayoutProvider.getLayoutView("category");

        Category category = categoryService.getCategoryBySlug(catSlug)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        "Nie znaleziono strony o adresie: /" + catSlug));

        if (subcatSlug != null) {
            Subcategory subCategory = subcategoryService.getSubcategory(catSlug, subcatSlug)
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                            "Nie znaleziono strony o adresie: /" + catSlug + "/" + subcatSlug));

            mav.addObject("subCategory", subCategory);
        }

        final List<Subcategory> subCategories = subcategoryService.getSubcategories(catSlug);
        final List<Product> products = productService.getProducts(catSlug, subcatSlug);

        mav
                .addObject("subCategories", subCategories)
                .addObject("products", products)
                .addObject("category", category);
        return mav;
    }
}
