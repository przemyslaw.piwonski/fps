package pl.sdacademy.fpsapp.domain.shoppingcart;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;
import pl.sdacademy.fpsapp.domain.common.BasicLayoutProvider;
import pl.sdacademy.fpsapp.domain.product.ProductService;
import pl.sdacademy.fpsapp.model.OrderLine;
import pl.sdacademy.fpsapp.model.Product;

import java.math.BigDecimal;

@Controller
@RequestMapping("/shopping-cart")
public class ShoppingCartController {
    private final ShoppingCart shoppingCart;
    private final ProductService productService;
    private final BasicLayoutProvider basicLayoutProvider;

    public ShoppingCartController(
            ShoppingCart shoppingCart,
            ProductService productService,
            BasicLayoutProvider basicLayoutProvider
    ) {
        this.shoppingCart = shoppingCart;
        this.productService = productService;
        this.basicLayoutProvider = basicLayoutProvider;
    }

    @PostMapping("/add-to-cart")
    public String addToCart(@RequestParam("productId") Integer productId) {
        final Product product = productService.getProduct(productId).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.BAD_REQUEST, "Nieznaleziono productu o id: " + productId));
        shoppingCart.addOrderLine(product);
        return "redirect:/shopping-cart";
    }

    @PostMapping("/increment-quantity")
    public String incQuantity(@RequestParam("productId") Integer productId) {
        shoppingCart.incQuantity(productId);
        return "redirect:/shopping-cart";
    }

    @PostMapping("/decrement-quantity")
    public String decQuantity(@RequestParam("productId") Integer productId) {
        shoppingCart.decQuantity(productId);
        return "redirect:/shopping-cart";
    }

    @PostMapping("/remove-from-cart")
    public String removeFromCart(@RequestParam("productId") Integer productId) {
        shoppingCart.removeOrderLine(productId);
        return "redirect:/shopping-cart";
    }

    @GetMapping
    public ModelAndView getCart() {
        final ModelAndView mav = basicLayoutProvider.getLayoutView("shopping-cart");
        BigDecimal summaryPrice = shoppingCart.getSummaryPrice();
        mav
                .addObject("summaryPrice", summaryPrice)
                .addObject("shoppingCart", shoppingCart);
        return mav;
    }
}
