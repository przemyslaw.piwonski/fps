package pl.sdacademy.fpsapp.domain.product;

import org.springframework.stereotype.Service;
import pl.sdacademy.fpsapp.model.Category;
import pl.sdacademy.fpsapp.model.Product;


import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> getProducts(String categorySlug, String subcategorySlug) {
        if (subcategorySlug == null) {
            return productRepository.findAllBySubCategory_ParentCategory_Slug(categorySlug);
        }
        return productRepository.findAllBySubCategory_Slug(subcategorySlug);
    }

    public Optional<Product> getProduct(Integer id) {
        return productRepository.findById(id);
    }
}
