package pl.sdacademy.fpsapp.domain.user;

import org.springframework.stereotype.Service;
import pl.sdacademy.fpsapp.domain.address.Address;
import pl.sdacademy.fpsapp.model.User;
import pl.sdacademy.fpsapp.model.enums.Role;

import javax.transaction.Transactional;

@Service
public class UserService {
    final private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Transactional
    public User addUser(String login, String password, Address address){
        if (userRepository.findUserByLogin(login)) return null;
        final User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        user.setAddress(address);
        user.setRole(Role.USER);
        return userRepository.save(user);
    }
}
