package pl.sdacademy.fpsapp.domain.category;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sdacademy.fpsapp.model.Category;

import java.util.Optional;

public interface CategoryRepository extends JpaRepository<Category, Integer> {
    Optional<Category> findBySlug(String slug);
}
