package pl.sdacademy.fpsapp.domain.common;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import pl.sdacademy.fpsapp.domain.category.CategoryService;

@Component
public class BasicLayoutProvider {
    private final static String DEFAULT_LAYOUT = "index";

    private final CategoryService categoryService;

    public BasicLayoutProvider(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public ModelAndView getLayoutView(String fragment) {
        return new ModelAndView(DEFAULT_LAYOUT)
                .addObject("categories", categoryService.getCategories())
                .addObject("fragment", fragment)
                .addObject("header", "header");
    }
}
