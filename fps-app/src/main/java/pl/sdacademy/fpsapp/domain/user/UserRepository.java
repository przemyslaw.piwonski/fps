package pl.sdacademy.fpsapp.domain.user;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sdacademy.fpsapp.model.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {

    boolean findUserByLogin(String login);
}
