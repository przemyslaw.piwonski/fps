package pl.sdacademy.fpsapp.domain.address;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public class Address {
    private final String regex = "\\b\\p{IsAlphabetic}{3,45}\\b";

    @NotBlank(message = "Dodaj imię")
    @Pattern(
            flags = Pattern.Flag.UNICODE_CASE,
            regexp = regex,
            message = "Imię musi zawierać przynajmniej trzy litery, bez odstępów, bez cyfr")
    private String firstName;

    @NotBlank(message = "Dodaj nazwisko")
    @Pattern(
            flags = Pattern.Flag.UNICODE_CASE,
            regexp = regex,
            message = "Nazwisko musi zawierać przynajmniej trzy litery, bez odstępów, bez cyfr"
    )
    private String lastName;

    @NotBlank(message = "Dodaj adres")
    @Pattern(
            flags = Pattern.Flag.UNICODE_CASE,
            regexp = "^.*?\\s[N]{0,1}([/a-zA-Z0-9]+)\\s*\\w*$",
            message = "Adres powinięn zawierać nazwę ulicy opcjonalnie numer budynku / numer lokalu"
    )
    private String address;

    @NotBlank(message = "Dodaj kod pocztowy 00-000")
    @Pattern(
            regexp = "\\d{2}-\\d{3}",
            message = "Format kodu pocztowego 00-000"
    )
    private String zipCode;

    @NotBlank(message = "Dodaj nazwę miasta")
    @Pattern(
            flags = Pattern.Flag.UNICODE_CASE,
            regexp = regex,
            message = "Nazwa musi zawierać przynajmniej trzy litery, bez odstępów, bez cyfr"
    )
    private String city;

    public String toString() {
        return firstName + " " +
                lastName + " " +
                address + " " +
                zipCode + " " +
                city;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
