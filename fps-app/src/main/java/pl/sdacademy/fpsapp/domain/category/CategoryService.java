package pl.sdacademy.fpsapp.domain.category;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import pl.sdacademy.fpsapp.model.Category;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryService {
    private final CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public Category addCategory(Category category) {
        return categoryRepository.save(category);
    }

    @Cacheable("categories")
    public List<Category> getCategories() {
        return categoryRepository.findAll();
    }

    @Cacheable("category")
    public Optional<Category> getCategoryBySlug(String catSlug) {
        return categoryRepository.findBySlug(catSlug);
    }

}
