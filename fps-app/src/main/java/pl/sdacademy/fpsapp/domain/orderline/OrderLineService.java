package pl.sdacademy.fpsapp.domain.orderline;

import org.springframework.stereotype.Service;
import pl.sdacademy.fpsapp.model.OrderLine;

import java.util.List;

@Service
public class OrderLineService {
    private final OrderLineRepository orderLineRepository;

    public OrderLineService(OrderLineRepository orderLineRepository) {
        this.orderLineRepository = orderLineRepository;
    }
    
    public List<OrderLine> addOrderLineList(List<OrderLine> orderLineList) {
        return orderLineRepository.saveAll(orderLineList);
    }
}
