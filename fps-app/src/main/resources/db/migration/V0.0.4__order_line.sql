create table order_line
(
	id serial not null
		constraint order_line_pk
			primary key,
	quantity int not null,
	price numeric not null,
	product_id int not null
		constraint order_line_product_id_fk
			references product
				on update cascade on delete restrict
);
