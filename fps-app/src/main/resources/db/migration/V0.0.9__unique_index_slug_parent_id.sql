drop index sub_category_name_uindex;

drop index sub_category_slug_uindex;

create unique index sub_category_slug_parent_id_uindex
    on sub_category (slug, parent_id);