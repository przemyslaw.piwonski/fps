alter table "order" rename to orders;

alter table order_line drop constraint order_line_order_fk;

alter table order_line
    add constraint order_line_order_fk
        foreign key (order_id) references orders (id)
            on update cascade on delete restrict;
