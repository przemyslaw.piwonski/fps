create table "order"
(
	id serial not null
		constraint order_pk
			primary key,
	date_time timestamp not null,
	delivery_address varchar not null,
	status varchar not null,
	order_line_id int not null
		constraint order_order_line_id_fk
			references order_line
				on update cascade on delete restrict
);
