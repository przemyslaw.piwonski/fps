create table product
(
    id serial not null
        constraint product_pk
            primary key,
    name varchar not null,
    url_photo varchar,
    price numeric not null,
    description varchar not null,
    category_id int not null
        constraint product_sub_category_id_fk
            references sub_category (id)
            on update cascade on delete restrict
);