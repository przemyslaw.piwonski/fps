alter table order_line
	add order_id int;

alter table order_line
	add constraint order_line_order_fk
		foreign key (order_id) references "order"
			on update cascade on delete restrict;
