create table shop_user
(
	id serial not null
		constraint users_pk
			primary key,
	login varchar not null,
	password varchar not null,
	address varchar,
	role varchar not null
);

