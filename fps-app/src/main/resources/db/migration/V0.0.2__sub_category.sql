create table sub_category
(
    id serial not null
        constraint sub_category_pk
            primary key,
    name varchar not null,
    slug varchar not null,
    parent_id int not null
        constraint sub_category_category_id_fk
            references category
            on update cascade on delete restrict
);

create unique index sub_category_name_uindex
    on sub_category (name);

create unique index sub_category_slug_uindex
    on sub_category (slug);

