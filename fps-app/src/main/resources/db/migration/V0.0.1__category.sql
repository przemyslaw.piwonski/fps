create table category
(
    id serial not null
        constraint category_pk
            primary key,
    name varchar not null,
    slug varchar not null
);

create unique index category_name_uindex
    on category (name);

create unique index category_slug_uindex
    on category (slug);