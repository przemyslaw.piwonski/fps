package pl.sdacademy.fpsapp.test;

import org.mockito.Mockito;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import pl.sdacademy.fpsapp.domain.category.CategoryService;

@TestConfiguration
public class ControllerTestConfiguration {
    @Bean
    public CategoryService categoryService() {
        return Mockito.mock(CategoryService.class);
    }
}
