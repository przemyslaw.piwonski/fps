package pl.sdacademy.fpsapp.domain.shoppingcart;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.servlet.ModelAndView;
import pl.sdacademy.fpsapp.domain.common.BasicLayoutProvider;
import pl.sdacademy.fpsapp.domain.product.ProductService;
import pl.sdacademy.fpsapp.model.Category;
import pl.sdacademy.fpsapp.model.Product;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ShoppingCartController.class)
class ShoppingCartControllerTest {

    @MockBean
    private ShoppingCart shoppingCart;

    @MockBean
    private ProductService productService;

    @MockBean
    private BasicLayoutProvider basicLayoutProvider;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void addToCartWillThrowNotFoundException() throws Exception {
        //given
        given(productService.getProduct(1)).willReturn(Optional.empty());

        //when
        final ResultActions resultActions = mockMvc.perform(
                post("/shopping-cart/add-to-cart")
                        .param("productId", "1")
                        .with(csrf()));

        //then
        resultActions
                .andExpect(status().isBadRequest());
    }

    @Test
    void addToCartWillRedirect() throws Exception {
        //given
        final Product product = new Product();
        product.setId(1);

        given(productService.getProduct(1)).willReturn(Optional.of(product));
        given(shoppingCart.isProductPresent(1)).willReturn(true);

        //when
        final ResultActions resultActions = mockMvc.perform(
                post("/shopping-cart/add-to-cart")
                        .param("productId", "1")
                        .with(csrf()));

        //then
        resultActions
                .andExpect(status().is3xxRedirection());
    }

    @Test
    void addToCart() throws Exception {
        //given
        final Product product = new Product();
        product.setId(1);

        given(productService.getProduct(1)).willReturn(Optional.of(product));
        given(shoppingCart.isProductPresent(1)).willReturn(false);

        //when
        final ResultActions resultActions = mockMvc.perform(
                post("/shopping-cart/add-to-cart")
                        .param("productId", "1")
                        .with(csrf()));

        //then
        resultActions
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/shopping-cart"));
    }

    @Test
    void incQuantity() throws Exception {
        //given

        //when
        final ResultActions resultActions = mockMvc.perform(
                post("/shopping-cart/increment-quantity")
                        .param("productId", "1")
                        .with(csrf())
        );

        //then
        resultActions
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/shopping-cart"));
    }

    @Test
    void decQuantity() throws Exception {
        //given

        //when
        final ResultActions resultActions = mockMvc.perform(
                post("/shopping-cart/decrement-quantity")
                        .param("productId", "1")
                        .with(csrf())
        );

        //then
        resultActions
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/shopping-cart"));
    }

    @Test
    void removeFromCart() throws Exception {
        //given

        //when
        final ResultActions resultActions = mockMvc.perform(
                post("/shopping-cart/remove-from-cart")
                        .param("productId", "1")
                        .with(csrf())
        );

        //then
        resultActions
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/shopping-cart"));
    }

    @Test
    void getCart() throws Exception {
        //given
        final BigDecimal summaryPrice = BigDecimal.valueOf(299.1);
        final List<Category> categories = Collections.emptyList();

        given(shoppingCart.getSummaryPrice()).willReturn(summaryPrice);
        given(basicLayoutProvider.getLayoutView("shopping-cart"))
                .willReturn(
                        new ModelAndView("index")
                                .addObject("categories", categories)
                                .addObject("fragment", "shopping-cart")
                                .addObject("header", "header")
                );

        //when
        final ResultActions resultActions = mockMvc.perform(
                get("/shopping-cart")
                        .accept(MediaType.TEXT_HTML));

        //then
        resultActions
                .andExpect(status().isOk())
                .andExpect(model().attribute("categories", categories))
                .andExpect(model().attribute("header", "header"))
                .andExpect(model().attribute("fragment", "shopping-cart"))
                .andExpect(model().attribute("summaryPrice", summaryPrice))
                .andExpect(model().attribute("shoppingCart", shoppingCart));
    }
}