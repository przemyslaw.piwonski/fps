package pl.sdacademy.fpsapp.domain.shoppingcart;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.sdacademy.fpsapp.model.OrderLine;
import pl.sdacademy.fpsapp.model.Product;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ShoppingCartTest {

    private ShoppingCart shoppingCart;

    @BeforeEach
    void setUp() {
        shoppingCart = new ShoppingCart();
    }

    @Test
    void getOrderLineList() {
        //given
        final List<OrderLine> expectedResult = Collections.synchronizedList(new LinkedList<>());
        //when
        var result = shoppingCart.getOrderLineList();
        //then
        assertEquals(expectedResult, result);
    }

    @Test
    void isProductPresentWithEmptyList() {
        //given
        final boolean expectedResult = false;
        final List<OrderLine> orderLineList = Collections.synchronizedList(new LinkedList<>());

        //when
        var result = shoppingCart.isProductPresent(1);

        //then
        assertEquals(expectedResult, result);

    }

    @Test
    void isProductPresent() {
        //given
        final Product product = new Product();
        product.setId(1);
        product.setName("some product");
        product.setPrice(BigDecimal.valueOf(123));

        //when
        var result1 = shoppingCart.isProductPresent(1);
        shoppingCart.addOrderLine(product);
        var result2 = shoppingCart.isProductPresent(1);

        //then
        assertFalse(result1);
        assertTrue(result2);
    }

    @Test
    void isProductPresentFalse() {
        //given
        final boolean expectedResult = false;
        final Product product = new Product();
        product.setId(2);
        product.setName("some product");
        product.setPrice(BigDecimal.valueOf(123));
        shoppingCart.addOrderLine(product);

        //when
        var result = shoppingCart.isProductPresent(1);

        //then
        assertEquals(expectedResult, result);
    }

    @Test
    void getSummaryPrice() {
        //given
        final BigDecimal expectedResult = new BigDecimal("8.99");
        final Product product = new Product();
        product.setId(1);
        product.setPrice(BigDecimal.valueOf(2.99));
        final Product product1 = new Product();
        product1.setId(2);
        product1.setPrice(BigDecimal.valueOf(3));
        shoppingCart.addOrderLine(product);
        shoppingCart.addOrderLine(product1);
        shoppingCart.addOrderLine(product1);

        //when
        var result = shoppingCart.getSummaryPrice();

        //then
        assertEquals(expectedResult, result);
    }


}