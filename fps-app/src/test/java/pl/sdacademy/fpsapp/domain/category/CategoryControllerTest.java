package pl.sdacademy.fpsapp.domain.category;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.servlet.ModelAndView;
import pl.sdacademy.fpsapp.domain.common.BasicLayoutProvider;
import pl.sdacademy.fpsapp.domain.product.ProductService;
import pl.sdacademy.fpsapp.domain.subcategory.SubcategoryService;
import pl.sdacademy.fpsapp.model.Category;
import pl.sdacademy.fpsapp.model.Product;
import pl.sdacademy.fpsapp.model.Subcategory;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(CategoryController.class)
class CategoryControllerTest {

    @MockBean
    private ProductService productService;

    @MockBean
    private CategoryService categoryService;

    @MockBean
    private SubcategoryService subCategoryService;

    @MockBean
    private BasicLayoutProvider basicLayoutProvider;

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("display name")
    void getCategory() throws Exception {
        //GIVEN
        List<Product> products = Collections.emptyList();
        List<Subcategory> subCategories = Collections.emptyList();
        Category pcGames = new Category();
        pcGames.setSlug("gry");
        List<Category> categories = List.of(pcGames);
        given(categoryService.getCategoryBySlug("gry")).willReturn(Optional.of(pcGames));
        given(basicLayoutProvider.getLayoutView("category"))
                .willReturn(
                        new ModelAndView("index")
                                .addObject("categories", categories)
                                .addObject("fragment", "category")
                                .addObject("header", "header")
                );
        given(subCategoryService.getSubcategories("gry")).willReturn(subCategories);
        given(productService.getProducts("gry", null)).willReturn(products);

        //WHEN
        final ResultActions resultActions = mockMvc.perform(
                get("/categories/{category-slug}", "gry")
                        .accept(MediaType.TEXT_HTML)
        );

        //THEN
        resultActions
                .andExpect(status().isOk())
                .andExpect(model().attribute("categories", categories))
                .andExpect(model().attribute("fragment", "category"))
                .andExpect(model().attribute("header", "header"))
                .andExpect(model().attribute("products", products))
                .andExpect(model().attribute("category", pcGames))
                .andExpect(model().attribute("subCategories", subCategories))
                .andExpect(view().name("index"));

    }

    @Test
    void getSubCategory() throws Exception {
        //GIVEN
        Category pcGames = new Category();
        pcGames.setSlug("gry");
        Subcategory rpgSubcat = new Subcategory();
        rpgSubcat.setSlug("rpg");
        rpgSubcat.setParentCategory(pcGames);

        List<Category> categories = List.of(pcGames);
        List<Product> products = Collections.emptyList();
        List<Subcategory> subCategories = List.of(rpgSubcat);

        given(basicLayoutProvider.getLayoutView("category"))
                .willReturn(
                        new ModelAndView("index")
                                .addObject("categories", categories)
                                .addObject("fragment", "category")
                                .addObject("header", "header")
                );

        given(categoryService.getCategoryBySlug("gry")).willReturn(Optional.of(pcGames));

        given(productService.getProducts(eq("gry"), eq(rpgSubcat.getSlug()))).willReturn(products);

        given(subCategoryService.getSubcategories("gry")).willReturn(subCategories);

        given(subCategoryService.getSubcategory(eq("gry"), eq("rpg"))).willReturn(Optional.of(rpgSubcat));

        //WHEN
        final ResultActions resultActions = mockMvc.perform(
                get("/categories/{category-slug}/{sub-category-slug}", "gry", "rpg")
                        .accept(MediaType.TEXT_HTML)
        );

        //THEN
        resultActions
                .andExpect(status().isOk())
                .andExpect(model().attribute("categories", categories))
                .andExpect(model().attribute("fragment", "category"))
                .andExpect(model().attribute("header", "header"))
                .andExpect(model().attribute("products", products))
                .andExpect(model().attribute("category", pcGames))
                .andExpect(model().attribute("subCategories", subCategories))
                .andExpect(model().attribute("subCategory", rpgSubcat))
                .andExpect(view().name("index"));
    }

}