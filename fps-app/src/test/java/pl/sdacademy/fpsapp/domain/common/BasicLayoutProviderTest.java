package pl.sdacademy.fpsapp.domain.common;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.servlet.ModelAndView;
import pl.sdacademy.fpsapp.domain.category.CategoryService;
import pl.sdacademy.fpsapp.model.Category;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;



@ExtendWith(MockitoExtension.class)
class BasicLayoutProviderTest {

    private BasicLayoutProvider basicLayoutProvider;

    @Mock
    private CategoryService categoryService;

    @BeforeEach
    void setUp() {
        basicLayoutProvider = new BasicLayoutProvider(categoryService);
    }

    @Test
    void shouldReturnModelAndView() {
        //GIVEN
        List<Category> categories = Collections.emptyList();
        given(categoryService.getCategories()).willReturn(categories);

        final ModelAndView expectedResult = new ModelAndView("index")
                .addObject("categories", categories)
                .addObject("fragment", "someFragment")
                .addObject("header", "header");

        //WHEN
        var result = basicLayoutProvider.getLayoutView("someFragment");

        //THEN
        assertEquals(expectedResult.getViewName(), result.getViewName());
        assertEquals(expectedResult.getModel().get("categories"), result.getModel().get("categories"));
        assertEquals(expectedResult.getModel().get("fragment"), result.getModel().get("fragment"));
        assertEquals(expectedResult.getModel().get("header"), result.getModel().get("header"));
    }

}