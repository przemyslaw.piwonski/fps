package pl.sdacademy.fpsapp.domain.orderline;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.sdacademy.fpsapp.model.OrderLine;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class OrderLineServiceTest {

    @Mock
    private OrderLineRepository orderLineRepository;

    private OrderLineService orderLineService;

    @BeforeEach
    void setUp() {
        orderLineService = new OrderLineService(orderLineRepository);
    }

    @Test
    public void shouldReturnOrderLineList() {
        //GIVEN
        final List<OrderLine> expectedResult = Collections.emptyList();
        given(orderLineRepository.saveAll(Collections.emptyList())).willReturn(expectedResult);

        //WHEN
        var result = orderLineService.addOrderLineList(Collections.emptyList());

        //THEN
        assertSame(expectedResult, result);
    }

}
