package pl.sdacademy.fpsapp.domain.order;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.sdacademy.fpsapp.domain.address.Address;
import pl.sdacademy.fpsapp.domain.orderline.OrderLineService;
import pl.sdacademy.fpsapp.domain.shoppingcart.ShoppingCart;
import pl.sdacademy.fpsapp.model.Order;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class OrderServiceTest {

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private ShoppingCart shoppingCart;

    @Mock
    private OrderLineService orderLineService;

    private OrderService orderService;

    @BeforeEach
    void setUp() {
        orderService = new OrderService(orderRepository, shoppingCart, orderLineService);
    }

    @Test
    void shouldAddOrder() {
        //GIVEN
        final Address address = new Address();

        final Order expectedResult = new Order();

        given(orderRepository.save(any())).willReturn(expectedResult);

        //WHEN
        var result = orderService.addOrder(address);

        //THEN
        assertSame(expectedResult, result);
    }
}