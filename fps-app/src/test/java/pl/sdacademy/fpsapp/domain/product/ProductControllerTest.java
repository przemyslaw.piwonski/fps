package pl.sdacademy.fpsapp.domain.product;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.servlet.ModelAndView;
import pl.sdacademy.fpsapp.domain.common.BasicLayoutProvider;
import pl.sdacademy.fpsapp.model.Category;
import pl.sdacademy.fpsapp.model.Product;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ProductController.class)
class ProductControllerTest {

    @MockBean
    private ProductService productService;

    @MockBean
    private BasicLayoutProvider basicLayoutProvider;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void getProduct() throws Exception {
        //given
        final List<Category> categories = Collections.emptyList();
        final Product product = new Product();
        product.setId(1);

        given(basicLayoutProvider.getLayoutView("product-details")).willReturn(
                new ModelAndView("index")
                        .addObject("categories", categories)
                        .addObject("header", "header")
                        .addObject("fragment", "product-details"));
        given(productService.getProduct(1)).willReturn(Optional.of(product));

        //when
        final ResultActions resultActions = mockMvc.perform(
                get("/products/{product-id}", "1")
                        .accept(MediaType.TEXT_HTML)
        );

        //then
        resultActions
                .andExpect(status().isOk())
                .andExpect(model().attribute("categories", categories))
                .andExpect(model().attribute("header", "header"))
                .andExpect(model().attribute("fragment", "product-details"))
                .andExpect(model().attribute("product", product));
    }

    @Test
    void getProductWillThrowNotFoundException() throws Exception {
        //given
        given(productService.getProduct(1)).willReturn(Optional.empty());

        //when
        final ResultActions resultActions = mockMvc.perform(
                get("/products/{product-id}", "1")
                        .accept(MediaType.TEXT_HTML)
        );

        //then
        resultActions
                .andExpect(status().isNotFound());
    }
}