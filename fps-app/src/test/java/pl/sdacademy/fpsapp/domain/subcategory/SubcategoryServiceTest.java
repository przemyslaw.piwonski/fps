package pl.sdacademy.fpsapp.domain.subcategory;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.sdacademy.fpsapp.model.Subcategory;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class SubcategoryServiceTest {

    @Mock
    private SubcategoryRepository subcategoryRepository;

    private SubcategoryService subcategoryService;

    @BeforeEach
    void setUp(){
        subcategoryService = new SubcategoryService(subcategoryRepository);
    }

    @Test
    void addSubcategory(){
        //given
        final Subcategory expectedResult = new Subcategory();
        final Subcategory subcategory = new Subcategory();
        given(subcategoryRepository.save(subcategory)).willReturn(expectedResult);

        //when
        var result = subcategoryService.addSubCategory(subcategory);

        //then
        assertSame(expectedResult, result);
    }

    @Test
    void getSubcategories() {
        //given
        final List<Subcategory> expectedResult = Collections.emptyList();
        given(subcategoryRepository.findAllByParentCategory_Slug("parent-category"))
                .willReturn(expectedResult);

        //when
        var result = subcategoryService.getSubcategories("parent-category");

        //then
        assertSame(expectedResult, result);
    }

    @Test
    void getSubcategory() {
        //given
        final Optional<Subcategory> expectedResult = Optional.empty();
        given(subcategoryRepository
                .findByParentCategory_SlugAndSlug("parent-category", "subcategory"))
                .willReturn(expectedResult);
        //when
        var result = subcategoryService
                .getSubcategory("parent-category", "subcategory");

        //then
        assertSame(expectedResult, result);
    }
}
