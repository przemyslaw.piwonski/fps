package pl.sdacademy.fpsapp.domain.order;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.servlet.ModelAndView;
import pl.sdacademy.fpsapp.domain.common.BasicLayoutProvider;
import pl.sdacademy.fpsapp.domain.shoppingcart.ShoppingCart;
import pl.sdacademy.fpsapp.model.Category;
import pl.sdacademy.fpsapp.model.OrderLine;
import pl.sdacademy.fpsapp.model.Product;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(OrderController.class)
class OrderControllerTest {

    @MockBean
    private ShoppingCart shoppingCart;

    @MockBean
    private OrderService orderService;

    @MockBean
    private BasicLayoutProvider basicLayoutProvider;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void orderForm() throws Exception {
        //GIVEN
        final List<Category> categories = Collections.emptyList();
        final Product product = new Product();
        product.setName("Some Product");
        final OrderLine orderLine = new OrderLine();
        orderLine.setProduct(product);
        orderLine.setPrice(BigDecimal.valueOf(22.22));
        given(basicLayoutProvider.getLayoutView("order-form"))
                .willReturn(
                        new ModelAndView("index")
                                .addObject("categories", categories)
                                .addObject("fragment", "order-form")
                                .addObject("header", "header")
                );

        given(shoppingCart.getOrderLineList()).willReturn(List.of(orderLine));

        //WHEN
        final ResultActions resultActions = mockMvc.perform(
                get("/order")
                        .accept(MediaType.TEXT_HTML)
        );

        //THEN
        resultActions
                .andExpect(status().isOk())
                .andExpect(model().attribute("categories", categories))
                .andExpect(model().attribute("fragment", "order-form"))
                .andExpect(model().attribute("header", "header"))
                .andExpect(model().attribute("shoppingCart", shoppingCart))
                .andExpect(view().name("index"));
    }

    @Test
    void orderFormWithEmptyOrderLineList() throws Exception {
        //GIVEN
        final List<Category> categories = Collections.emptyList();

        given(basicLayoutProvider.getLayoutView("order-form"))
                .willReturn(
                        new ModelAndView("index")
                                .addObject("categories", categories)
                                .addObject("fragment", "order-form")
                                .addObject("header", "header")
                );

        given(shoppingCart.getOrderLineList()).willReturn(Collections.emptyList());

        //WHEN
        final ResultActions resultActions = mockMvc.perform(
                get("/order")
                        .accept(MediaType.TEXT_HTML)
        );

        //THEN
        resultActions
                .andExpect(status().is3xxRedirection());
    }

    @Test
    void thanksPageShouldRedirect() throws Exception {
        //GIVEN
        final List<Category> categories = Collections.emptyList();

        given(basicLayoutProvider.getLayoutView("thank-you-page")).willReturn(
                new ModelAndView("index")
                        .addObject("categories", categories)
                        .addObject("fragment", "thank-you-page")
                        .addObject("header", "header"));

        given(shoppingCart.getOrderLineList()).willReturn(Collections.emptyList());

        //WHEN
        final ResultActions resultActions = mockMvc.perform(
                get("/thanks")
                        .accept(MediaType.TEXT_HTML)
        );

        //THEN
        resultActions
                .andExpect(status().is3xxRedirection());
    }

    @Test
    void thanksPage() throws Exception {
        //GIVEN
        final List<Category> categories = Collections.emptyList();
        final Product product = new Product();
        product.setName("Some Product");
        final OrderLine orderLine = new OrderLine();
        orderLine.setProduct(product);
        orderLine.setPrice(BigDecimal.valueOf(22.22));
        given(basicLayoutProvider.getLayoutView("thank-you-page")).willReturn(
                new ModelAndView("index")
                        .addObject("categories", categories)
                        .addObject("fragment", "thank-you-page")
                        .addObject("header", "header"));

        given(shoppingCart.getOrderLineList()).willReturn(
                Collections.synchronizedList(new LinkedList<>(List.of(orderLine))));

        //WHEN
        final ResultActions resultActions = mockMvc.perform(
                get("/thanks")
                        .accept(MediaType.TEXT_HTML)
        );

        //THEN
        resultActions
                .andExpect(status().isOk())
                .andExpect(model().attribute("categories", categories))
                .andExpect(model().attribute("fragment", "thank-you-page"))
                .andExpect(model().attribute("header", "header"))
                .andExpect(view().name("index"));
    }

    @Test
    void addOrder() throws Exception {
        //GIVEN
        final List<Category> categories = Collections.emptyList();
        given(basicLayoutProvider.getLayoutView("order-form")).willReturn(
                new ModelAndView("index")
                        .addObject("categories", categories)
                        .addObject("fragment", "order-form")
                        .addObject("header", "header"));

        //WHEN
        final ResultActions resultActions = mockMvc.perform(
                post("/order")
                        .param("firstName", "firstName")
                        .param("lastName", "lastName")
                        .param("address", "ul. Fiołkowa 1")
                        .param("city", "Warszawa")
                        .param("zipCode", "00-000")
                        .with(csrf())
        );

        //THEN
        resultActions
                .andExpect(status().is3xxRedirection());
//                .andExpect(redirectedUrl("/thanks"));
    }

    @Test
    void addOrderWithBindingResultErrors() throws Exception {
        //GIVEN
        final List<Category> categories = Collections.emptyList();
        given(basicLayoutProvider.getLayoutView("order-form")).willReturn(
                new ModelAndView("index")
                        .addObject("categories", categories)
                        .addObject("fragment", "order-form")
                        .addObject("header", "header"));

        //WHEN
        final ResultActions resultActions = mockMvc.perform(
                post("/order")
                        .param("firstName", "firstName1")
                        .param("lastName", "lastName1")
                        .param("address", "ul. Fiołkowa 1///")
                        .param("city", "Warszawa1")
                        .param("zipCode", "00-0001")
                        .with(csrf())
        );

        //THEN
        resultActions
                .andExpect(status().isOk())
                .andExpect(model().attribute("categories", categories))
                .andExpect(model().attribute("fragment", "order-form"))
                .andExpect(model().attribute("header", "header"))
                .andExpect(model().attribute("shoppingCart", shoppingCart))
                .andExpect(view().name("index"));
    }
}