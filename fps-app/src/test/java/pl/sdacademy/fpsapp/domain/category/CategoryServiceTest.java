package pl.sdacademy.fpsapp.domain.category;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.sdacademy.fpsapp.model.Category;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class CategoryServiceTest {

    @Mock
    private CategoryRepository categoryRepository;

    private CategoryService categoryService;

    @BeforeEach
    void setUp() {
        categoryService = new CategoryService(categoryRepository);
    }

    @Test
    void addCategory() {
        //GIVEN
        final Category expectedResult = new Category();

        given(categoryRepository.save(expectedResult)).willReturn(expectedResult);

        //WHEN
        var result = categoryService.addCategory(expectedResult);

        //THEN
        assertSame(expectedResult, result);
    }

    @Test
    void getAllCategories() {
        //GIVEN
        final List<Category> expectedResult = Collections.emptyList();

        given(categoryRepository.findAll()).willReturn(expectedResult);

        //WHEN
        var result = categoryService.getCategories();

        //THEN
        assertSame(expectedResult, result);
    }

    @Test
    void getCategoryBySlug() {
        //GIVEN
        final Category category = new Category();
        category.setSlug("rpg");
        final Optional<Category> expectedResult = Optional.of(category);

        given(categoryRepository.findBySlug("rpg")).willReturn(expectedResult);

        //WHEN
        var result = categoryService.getCategoryBySlug("rpg");

        //THEN
        assertSame(expectedResult, result);
    }

}