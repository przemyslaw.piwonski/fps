package pl.sdacademy.fpsapp.domain.product;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.sdacademy.fpsapp.model.Product;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class ProductServiceTest {

    @Mock
    private ProductRepository productRepository;

    private ProductService productService;

    @BeforeEach
    void setUp() {
        productService = new ProductService(productRepository);
    }

    @Test
    void shouldReturnAllProductsByCategory() {
        //GIVEN
        final List<Product> expectedResult = Collections.emptyList();
        given(productRepository.findAllBySubCategory_ParentCategory_Slug("gry")).willReturn(expectedResult);

        //WHEN
        var result = productService.getProducts("gry", null);

        //THEN
        assertSame(expectedResult, result);
    }

    @Test
    void shouldReturnAllProductsByCategoryAndSubcategory() {
        //GIVEN
        final List<Product> expectedResult = Collections.emptyList();
        given(productRepository.findAllBySubCategory_Slug("rpg")).willReturn(expectedResult);

        //WHEN
        var result = productService.getProducts("gry", "rpg");

        //THEN
        assertSame(expectedResult, result);
    }

    @Test
    void shouldReturnSingleOptionalProduct() {
        //given
        final Optional<Product> expectedResult = Optional.of(new Product());
        given(productRepository.findById(1)).willReturn(expectedResult);

        //when
        var result = productService.getProduct(1);

        //then
        assertSame(expectedResult, result);
    }
}