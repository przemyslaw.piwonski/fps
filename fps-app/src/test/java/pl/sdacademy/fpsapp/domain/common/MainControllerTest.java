package pl.sdacademy.fpsapp.domain.common;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.servlet.ModelAndView;
import pl.sdacademy.fpsapp.model.Category;

import java.util.Collections;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(MainController.class)
class MainControllerTest {

    @MockBean
    private BasicLayoutProvider basicLayoutProvider;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void getHomeView() throws Exception {
        //GIVEN
        List<Category> categories = Collections.emptyList();

        given(basicLayoutProvider.getLayoutView(
                "welcome"
        )).willReturn(new ModelAndView("index")
                .addObject("categories", categories)
                .addObject("fragment", "welcome")
                .addObject("header", "header")
        );

        //WHEN
        final ResultActions resultActions = mockMvc.perform(
                get("/")
                        .accept(MediaType.TEXT_HTML)
        );

        //THEN
        resultActions
                .andExpect(status().isOk())
                .andExpect(model().attribute("categories", categories))
                .andExpect(model().attribute("fragment", "welcome"))
                .andExpect(model().attribute("header", "header"))
                .andExpect(view().name("index"));
    }
}